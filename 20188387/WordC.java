package softwstudy;

import java.io.BufferedReader;  
import java.io.FileReader;  
import java.util.ArrayList;  
import java.util.Collections;  
import java.util.Comparator;  
import java.util.List;  
import java.util.Map;  
import java.util.Map.Entry;  
import java.util.TreeMap;  
  
public class WordC {  
  
    public static void main(String[] args) throws Exception {  
          
        BufferedReader bufferr = new BufferedReader(new FileReader("C:/test/wordcount test.txt"));  
        List<String> lists = new ArrayList<String>();          //A list of store filtered words  
        String readLine = null;
        int countword = 0;
		while((readLine = bufferr.readLine()) != null){  
            String[] wordsArr = readLine.split("[^a-zA-Z0-9]");  //Filter out letters and numbers only
            for (String word : wordsArr) {  
            	String text = word.toLowerCase();
                if(text.length() >=4){             //Remove words less than 4 letters in length 
                    lists.add(text);  
                    countword ++;
                }  
            }  
        }  
		System.out.println("words:" + countword); 
        bufferr.close();  
          
        Map<String, Integer> wordsCount = new TreeMap<String,Integer>();  //Store word count information, key value is word, value is word count      
          
        //Word frequency statistics  
        for (String li : lists) {  
            if(wordsCount.get(li) != null){  
                wordsCount.put(li,wordsCount.get(li) + 1);  
            }else{  
                wordsCount.put(li,1);  
            }  
  
        }  
          
        SortMap(wordsCount);    //Sort by value 
      
    }  
      
    
    public static void SortMap(Map<String,Integer> oldmap){                 //Sort by value's value 
          
        ArrayList<Map.Entry<String,Integer>> list = new ArrayList<Map.Entry<String,Integer>>(oldmap.entrySet());  
          
        Collections.sort(list,new Comparator<Map.Entry<String,Integer>>(){  
            @Override  
            public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {  
                return o2.getValue() - o1.getValue();      //descending sort  
            }  
        });  
          
        for(int i = 0; i<10; i++){  
            System.out.println(list.get(i).getKey()+ ": " +list.get(i).getValue());  
        }     
    }  
  
} 
